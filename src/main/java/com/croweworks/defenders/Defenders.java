package com.croweworks.defenders;

import com.croweworks.defenders.block.DefendersCoreBlocks;
import com.croweworks.defenders.block.DefendersTrapBlocks;
import com.croweworks.defenders.item.DefenderItems;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod(Defenders.MOD_ID)
public class Defenders
{
    public static final Logger LOGGER = LogManager.getLogger();
    public static final String MOD_ID = "defenders";

    public Defenders() {
        final IEventBus bus = FMLJavaModLoadingContext.get().getModEventBus();
        bus.addListener(this::setup);

        DefenderItems.register(bus);
        DefendersCoreBlocks.register(bus);
//        DefendersTrapBlocks.register(bus);

        MinecraftForge.EVENT_BUS.register(this);
    }

    private void setup(final FMLCommonSetupEvent event)
    {

    }
}
