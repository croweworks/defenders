package com.croweworks.defenders.block;

import com.croweworks.defenders.Defenders;
import com.croweworks.defenders.block.traps.FireTrapBlock;
import com.croweworks.defenders.block.traps.SlowTrapBlock;
import com.croweworks.defenders.item.DefenderItems;
import com.croweworks.defenders.item.DefendersItemGroup;
import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraftforge.common.ToolType;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

import java.util.function.Supplier;

public class DefendersCoreBlocks extends AbstractDefendersBlocks {

    protected static final DeferredRegister<Block> BLOCKS
            = DeferredRegister.create(ForgeRegistries.BLOCKS, Defenders.MOD_ID);

    protected static <T extends Block> RegistryObject<T> registerBlock(final String name, final Supplier<T> block) {
        RegistryObject<T> toReturn = BLOCKS.register(name, block);
        registerBlockItem(name, toReturn);
        return toReturn;
    }

    protected static <T extends Block> void registerBlockItem(final String name, final RegistryObject<T> block) {
        DefenderItems.ITEMS.register(name, () -> new BlockItem(block.get(),
                new Item.Properties().group(DefendersItemGroup.DEFENDERS_ITEM_GROUP)));
    }

    public static void register(final IEventBus eventBus) {
        BLOCKS.register(eventBus);
    }

    public static final RegistryObject<Block> AMETHYST_ORE = registerBlock("amethyst_ore",
            () -> new Block(AbstractBlock.Properties.create(Material.ROCK)
                    .harvestLevel(2).setRequiresTool().harvestTool(ToolType.PICKAXE).hardnessAndResistance(5f)));

    public static final RegistryObject<Block> AMETHYST_BLOCK = registerBlock("amethyst_block",
            () -> new Block(AbstractBlock.Properties.create(Material.ROCK)
                    .harvestLevel(2).setRequiresTool().harvestTool(ToolType.PICKAXE).hardnessAndResistance(8f)));

    public static final RegistryObject<Block> FIRE_TRAP_BLOCK = registerBlock(FireTrapBlock.NAME,
            () -> new FireTrapBlock(AbstractBlock.Properties.create(Material.IRON)
                    .harvestLevel(2).hardnessAndResistance(6f).harvestTool(ToolType.PICKAXE).setRequiresTool()));

    public static final RegistryObject<Block> SLOW_TRAP_BLOCK = registerBlock(SlowTrapBlock.NAME,
            () -> new SlowTrapBlock(AbstractBlock.Properties.create(Material.IRON)
                    .harvestLevel(2).hardnessAndResistance(6f).harvestTool(ToolType.PICKAXE).setRequiresTool()));
}
