package com.croweworks.defenders.block.traps;

import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class SlowTrapBlock extends Block {

    public static final String NAME = "slow_trap_block";

    public SlowTrapBlock(final Properties properties) {
        super(properties);
    }

    @Override
    public void onEntityWalk(final World worldIn, final BlockPos pos, final Entity entity) {
        if (entity instanceof LivingEntity) {
            final LivingEntity livingEntity = (LivingEntity)entity;
            livingEntity.addPotionEffect(new EffectInstance(Effects.SLOWNESS, 40));
        }
        super.onEntityWalk(worldIn, pos, entity);
    }
}
