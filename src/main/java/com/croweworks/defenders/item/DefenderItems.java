package com.croweworks.defenders.item;

import com.croweworks.defenders.Defenders;
import net.minecraft.item.Item;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class DefenderItems {

    public static final DeferredRegister<Item> ITEMS =
            DeferredRegister.create(ForgeRegistries.ITEMS, Defenders.MOD_ID);

    public static final RegistryObject<Item> AMETHYST = ITEMS.register("amethyst",
            () -> new Item(new Item.Properties().group(DefendersItemGroup.DEFENDERS_ITEM_GROUP)));

    public static void register(final IEventBus eventBus) {
        ITEMS.register(eventBus);
    }
}
