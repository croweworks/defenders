package com.croweworks.defenders.item;

import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;

public class DefendersItemGroup {

    public static final ItemGroup DEFENDERS_ITEM_GROUP = new ItemGroup("defendersTab") {
        @Override
        public ItemStack createIcon() {
            return new ItemStack(DefenderItems.AMETHYST.get());
        }
    };

}
